;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                              ;
; Author    :   Arvind S Raj                                                   ;
; Date      :   16/10/2012                                                     ;
; Program   :   Hello world bootloader                                         ;
;                                                                              ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BITS 16                     ; Indicates the following code is 16 bit ASM code

jmp main                    ; Jump to main routine
nop                         ; No operations beyond this

main:
    mov ax, 07C0h           ; Setup 4k bytes of stack space after the bootloader
    add ax, 288             ; code
    mov ss, ax
    mov sp, 4096

    mov ax, 07C0h           ; Data segment points to the start of the bootloader
    mov ds, ax              ; code
    call PrintHelloWorld    ; Call the print routine
    jmp .InfiniteLoop

    .InfiniteLoop:          ; After printing Hello World, bootloader will jump
        jmp .InfiniteLoop   ; into this infinite loop


; Defining the string the bootloader will print

HelloWorld      db  "Hello World. This is from the bootloader", 0x0d, 0x0a, 0x00


PrintHelloWorld:            ; Routine that greets the world
    mov si, HelloWorld      ; Move string into si
    call PrintStr           ; Call the routine that prints the string
    ret


PrintStr:                   ; Routine to print string in SI
    push ax                 ; Save value of accumulator on the stack
    mov ah, 0Eh             ; 0Eh is the code of the print function of BIOS

    .loop:
        lodsb               ; Store byte from string in al
        cmp al, 0x00        ; If character is 0, exit
        je .done
        int 10h             ; BIOS video interrupt
        jmp .loop
    
    .done:
        pop ax              ; Restore value of accumulator
        ret


times 510-($-$$) db 0       ; Fill rest of bytes of BIOS with 0 bytes
dw 0xAA55                   ; Standard PC boot signature
